#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>
#include <cuda_runtime.h>


// counting all the number of neighbour celles 
__device__
int countNumberOfNeighbours(int col1, int row1, int row, int col,int *world){
	int count = 0;
	for(int i=-1; i<2; i++)
		for(int j=-1; j <2;j++ ){
			int my_row;
			int my_col;

			my_row = row1 + i;
			my_col = col1 + j;

			if(my_col == -1)
			my_col = col - 1;
                         
			if(my_row == -1)
			my_row = row -1;
                         
			if(my_col == col)
			my_col = 0;
                         

			if(my_row == row)
			my_row = 0;
                         
			if(i == 0 && j == 0){
			continue;
                           } 
			
			else{
	                   count += world[(my_col) * row + my_row];

			}

		}

	return count;
}


// checking the count of neighbour cells and replacing live cell to 1 and dead cell to 0
__global__  void countNeighbourCells(int row, int col, int *world,int *otherWorld){

         
       int idx = blockIdx.x * blockDim.x  + threadIdx.x;
         if(idx >= row *col){
           return;
	}	
           int row1 = idx/ row;
           int col1 = idx%row;
           otherWorld[idx] = countNumberOfNeighbours(row1,col1,row,col,world);

               __syncthreads();

              if(otherWorld[idx] == 3){
                     world[idx] =1;
                            }
			 else if(otherWorld[idx] != 2){
                               
				world[idx] = 0;
                        }
                   }




int main(){
const	int row = 10;
const	int col = 10;
	int *P1, *P2;
 int N = ((row*col)/1024)+1 ;
 cudaMallocManaged(&P1, row*col*sizeof(int));
 cudaMallocManaged(&P2, row*col*sizeof(int));
    
	for(int i=0; i < row*col; i++)
      {
		P1[i] = 0;
		P2[i] = 0;
	}

	//initialising values in grid

	P1[1 * row + 0] =1;
	P1[1 * row + 2] =1;
	P1[2] =1;
	P1[2 * row + 1] =1;
	P1[2 * row + 2] =1;


	for(int i = 0; i <100; i++)
		{
		for(int r =0; r < row; r++)
         {
			for(int c=0; c < col; c++)
				printf("%d",P1[c * row + r]);
			    printf("\n");
		         }

		fflush(stdout);
		countNeighbourCells<<<N,1024>>>(row,col,P1,P2);
        cudaDeviceSynchronize();
        usleep(100000);
        printf("\n");

	}

	cudaFree(P1);
	cudaFree(P2);

}